import React from 'react';
import './Card.css';

function Card(props) {
  return (
    <dl className='Card'>
      <dt className='Card__term'>{props.term}</dt>
      <dd className='Card__translation'>
        <input
          type='checkbox'
          id='Pinner'
          checked={props.pinnedStatus}
          onChange={props.onChange}
        />
        <label
          className='Button Button__Pinner'
          htmlFor='Pinner' >
          <span>Pin this card</span>
        </label>
        {props.definition}
      </dd>
    </dl>
  );
}

export default Card;
