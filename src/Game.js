import React, { Component } from 'react';
import Card from './Card';
import Revealer from './Revealer';
import NextButton from './NextButton';
import './Game.css';

class Game extends Component {
  constructor(props) {
    super(props)

    this.state = {
      cards: [],
      currentCardNumber: 0,
      isLoaded: false,
      error: null,
      revealTranslation: false,
      pinnedCards: []
    }
  }

  componentDidMount() {
    this.fetchCardsJson();
  }

  fetchCardsJson() {
    const dataAddress = './moj_rijecnik.json';

    fetch(dataAddress)
      .then(
        res => res.json()
      ).then( (result) => {
        this.setState({
          isLoaded: true,
          cards: shuffleCards(
            this.extractCardsFromJson(result)
          )
        });
      }, (error) => {
          this.setState({ isLoaded: true, error: error });
        }
      );
  }

  extractCardsFromJson(json) {
    const pairs = Object.entries(json);
    let cards = [];

    for (let i in pairs) {
      cards.push(
        {
          na_nasem: pairs[i][0],
          english: pairs[i][1]
        }
      );
    }

    return cards;
  }

  toggleReveal() {
    this.setState({
      revealTranslation: !this.state.revealTranslation
    })
  }

  togglePinnedCard() {
    const pinnedCards = this.state.pinnedCards.slice();
    let currentCard = this.state.cards[this.state.currentCardNumber];

    const visibleIndexInPinned = pinnedCards.findIndex(
      card => card.na_nasem === currentCard.na_nasem
    );

    if (visibleIndexInPinned === -1) {
      currentCard.isPinned = true;
      pinnedCards.push(currentCard);
    } else {
      currentCard.isPinned = false;
      pinnedCards.splice(visibleIndexInPinned, 1)
    }

    this.setState({pinnedCards: pinnedCards})
  }

  randomPinnedCard() {
    return this.state.pinnedCards[
      Math.floor(Math.random() * this.state.pinnedCards.length)
    ];
  }

  deferCardForRandomPinnedCard(cardToDefer) {
    let currentCards = this.state.cards.slice();
    // add the card to defer to the end
    currentCards.push(currentCards[cardToDefer])
    // Insert a random pinned card in it's place
    currentCards[cardToDefer] = this.randomPinnedCard()

    this.setState({cards: currentCards});
  }

  nextCardNumber() {
    const visibleCardNumber = this.state.currentCardNumber;
    const totalCardCount = this.state.cards.slice().length;
    let cardNumber = 0;

    if (visibleCardNumber !== totalCardCount - 1) {
      cardNumber = visibleCardNumber + 1;

      // 10% chance of getting random pinned card
      if (.90 < Math.random() && this.state.pinnedCards.length) {
        this.deferCardForRandomPinnedCard(cardNumber);
      }
    }

    return cardNumber;
  }

  changeCard() {
    if (this.nextCardNumber() === 0) {
      this.setState({
        cards: shuffleCards(this.state.cards)
      });
    }

    this.setState({
      currentCardNumber: this.nextCardNumber(),
      revealTranslation: false
    });
  }

  render() {
    const isLoaded = this.state.isLoaded;
    const error = this.state.error;
    const cards = this.state.cards;
    const currentCard = cards[this.state.currentCardNumber];
    let translation = 'Neznam :)';

    if (currentCard && currentCard.english !== '') {
      translation = currentCard.english;
    }

    if (error) {
      return <p className='errorMessage'>Error: {error.message}</p>;
    } else if (!isLoaded) {
      return <p className='LoadingMessage'>Sačekaj sekundu...</p>
    } else {
      return (
        <div className='Game'>
          <Revealer
            checked={this.state.revealTranslation}
            onChange={() => this.toggleReveal()}
          />
          <NextButton onClick={() => this.changeCard()}/>
          <Card
            term={currentCard.na_nasem}
            definition={translation}
            pinnedStatus={(currentCard.isPinned === true)}
            onChange={() => this.togglePinnedCard()}
          />
        </div>
      );
    }
  }
}

// based on https://bost.ocks.org/mike/shuffle/
function shuffleCards(array) {
  let shuffledCards = array.slice(),
      m = shuffledCards.length,
      t,
      i;

  // While there are cards to shuffles...
  while (m) {
    // Pick a remaining card at random...
    i = Math.floor(Math.random() * m--);
    // And swap it with the current card.
    t = shuffledCards[m];
    shuffledCards[m] = shuffledCards[i];
    shuffledCards[i] = t;
  }

  // Set the new order to state
  return shuffledCards;
}

export default Game;
