import React from 'react';

function NextButton(props) {
  return (
    <React.Fragment>
      <button className="Button Button__nextCard" onClick={props.onClick}>
        Sljedeći
      </button>
    </React.Fragment>
  )
}

export default NextButton;
