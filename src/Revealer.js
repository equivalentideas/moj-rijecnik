import React from 'react';

function Revealer(props) {
  return (
    <React.Fragment>
      <input
        type='checkbox'
        id='Revealer'
        checked={props.checked}
        onChange={props.onChange}
      />
      <label
        className='Button Button__Revealer'
        htmlFor='Revealer' >
        Reći mi
      </label>
    </React.Fragment>
  );
}

export default Revealer;
